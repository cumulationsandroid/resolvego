package com.resolvegital.disc

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.graphics.Color
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.resolvegital.disc.listener.ItemClickListener

class BleDeviceAdapter(val context: Context, val listener: ItemClickListener)

    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var bluetoothDevicesAddress: ArrayList<String> = ArrayList()
    val bluetoothDeviceMap: HashMap<String, BluetoothDevice> = HashMap()

    private var connectedDeviceAddress: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.ble_device_item_layout, parent, false)

        var holder = BleDeviceViewHolder(view)
        return holder;
    }

    fun addDevice(bluetoothDevice: BluetoothDevice) {
        if (bluetoothDeviceMap.put(bluetoothDevice.address, bluetoothDevice) == null) {
            bluetoothDevicesAddress = ArrayList(bluetoothDeviceMap.keys)
        }
    }

    override fun getItemCount(): Int {
        return bluetoothDevicesAddress.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val bluetoothDevice = bluetoothDeviceMap[bluetoothDevicesAddress.get(position)]
        val viewHolder = holder as BleDeviceViewHolder

        if (bluetoothDevice?.name != null) {
            viewHolder.deviceNameTv.text = bluetoothDevice?.name
        } else {
            viewHolder.deviceNameTv.text = bluetoothDevice?.address
        }

        if (connectedDeviceAddress != null && connectedDeviceAddress.equals(bluetoothDevice?.address)) {
            viewHolder.image_iv.setImageResource(R.drawable.ic_check_circle)
        } else {
            viewHolder.image_iv.setImageResource(R.drawable.signal_4)
        }
    }

    inner class BleDeviceViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        var deviceNameTv: TextView
        var image_iv:ImageView

        init {
            deviceNameTv = view.findViewById(R.id.device_name_tv)
            image_iv=view.findViewById(R.id.image_iv)
            view.findViewById<LinearLayout>(R.id.item_layout_ll).setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            listener.onItemClick(adapterPosition)
        }
    }

    fun getBluetoothDeviceForPostion(postion: Int): BluetoothDevice? {
        return bluetoothDeviceMap.get(bluetoothDevicesAddress.get(postion))
    }

    fun getBlueDevice(address: String): BluetoothDevice? {
        return bluetoothDeviceMap.get(address)
    }

    fun setConnectedDeviceAddress(address: String?) {
        connectedDeviceAddress = address
        notifyDataSetChanged()
    }

    fun getConnectedDeviceAddress(): String? {
        return connectedDeviceAddress
    }

}