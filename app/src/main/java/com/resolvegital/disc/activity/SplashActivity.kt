package com.resolvegital.disc.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.resolvegital.disc.R

class SplashActivity : BaseActivity() {

    lateinit var handler: Handler
    lateinit var sharedPreferences: SharedPreferences
    lateinit var context: Context

    var runnable: Runnable = object : Runnable {
        override fun run() {
            var intent = Intent(context, BleDeviceConnectionActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        handler = Handler()
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        handler.postDelayed(runnable, 3000)
    }
}