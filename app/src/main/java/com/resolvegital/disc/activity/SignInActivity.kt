package com.resolvegital.disc.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.resolvegital.disc.R

class SignInActivity : BaseActivity(), View.OnClickListener {

    lateinit var usernameEt: TextView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        context = this
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        findViewById<Button>(R.id.signin_tv_submit).setOnClickListener(this)
        usernameEt = findViewById(R.id.signin_et_username)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.signin_tv_submit -> {
                if (!usernameEt.text.toString().trim().isEmpty()) {

                    sharedPreferences.edit().putString("name", usernameEt.text.toString().trim()).apply()

                    var intent = Intent(context, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                else{
                    Toast.makeText(context,"Enter name",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}