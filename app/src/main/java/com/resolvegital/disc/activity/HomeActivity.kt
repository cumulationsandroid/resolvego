package com.resolvegital.disc.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.resolvegital.disc.R
import com.resolvegital.disc.fragment.GraphFragment
import com.resolvegital.disc.fragment.HomeFragment
import com.resolvegital.disc.fragment.SettingFragment
import android.content.Intent




class HomeActivity : BaseActivity(), View.OnClickListener
{
    lateinit var homeIv:TextView
    lateinit var graphIv:TextView
    lateinit var settingIv:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        homeIv=findViewById(R.id.home_iv_home)
        graphIv=findViewById(R.id.home_iv_graph)
        settingIv=findViewById(R.id.home_iv_setting)

        findViewById<TextView>(R.id.home_iv_home).setOnClickListener(this)
        findViewById<TextView>(R.id.home_iv_graph).setOnClickListener(this)
        findViewById<TextView>(R.id.home_iv_setting).setOnClickListener(this)

        findViewById<TextView>(R.id.home_iv_home).performClick()
    }

    override fun onClick(v: View?)
    {
        homeIv.isSelected=false
        graphIv.isSelected=false
        settingIv.isSelected=false

        when (v?.id) {
            R.id.home_iv_home -> {
                homeIv.isSelected=true
                var homeFragment = HomeFragment()
                loadFragment(homeFragment)
            }
            R.id.home_iv_setting -> {
                settingIv.isSelected=true
                var settingFragment = SettingFragment()
                loadFragment(settingFragment)
            }
            R.id.home_iv_graph -> {
                graphIv.isSelected=true
                var graphFragment = GraphFragment()
                loadFragment(graphFragment)
            }
        }
    }

    fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .add(R.id.main_container, fragment)
                .commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

}