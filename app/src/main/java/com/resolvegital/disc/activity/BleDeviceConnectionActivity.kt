package com.resolvegital.disc.activity

import android.Manifest
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.resolvegital.disc.R
import android.bluetooth.BluetoothDevice
import android.content.*
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import com.resolvegital.disc.BleDeviceAdapter
import android.os.IBinder
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.resolvegital.disc.listener.ItemClickListener
import com.resolvegital.disc.service.BluetoothService
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings


class BleDeviceConnectionActivity : BaseActivity() {

    lateinit var bluetoothAdapter: BluetoothAdapter
    private val REQUEST_LOCATION: Int = 111
    private val REQUEST_ENABLE_BT: Int = 112
    private val REQUEST_LOCATION_SETTING: Int=114

    private var handler: Handler = Handler()
    private val SCAN_PERIOD: Long = 10000
    private var mScanning: Boolean = false

    lateinit var blueDeviceRv: RecyclerView
    lateinit var connectBt: Button
    lateinit var bleDeviceAdapter: BleDeviceAdapter
    private val TAG: String = BleDeviceConnectionActivity::class.java.simpleName
    private var mBluetoothLeService: BluetoothService? = null

    val ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED"
    val ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED"
    val ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED"
    val ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE"
    val EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA"

    lateinit var sharedPreferences: SharedPreferences

    private var mGattReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.getAction();
            runOnUiThread(Runnable
            {
                val address = intent?.getStringExtra("ID")
                if (address != null) {
                    val connectedDevice = bleDeviceAdapter.getBlueDevice(address!!)

                    if (action.equals(ACTION_GATT_CONNECTED)) {
                        bleDeviceAdapter.setConnectedDeviceAddress(connectedDevice?.address!!)
                        /*   if (connectedDevice?.name != null) {
                               Toast.makeText(context, "Connected to Device" + connectedDevice?.name, Toast.LENGTH_SHORT).show()
                           } else {
                               Toast.makeText(context, "Connected to Device" + connectedDevice?.address, Toast.LENGTH_SHORT).show()
                           }*/
                    } else if (action.equals(ACTION_GATT_DISCONNECTED)) {
                        if (bleDeviceAdapter.getConnectedDeviceAddress().equals(connectedDevice?.address)!!) {
                            bleDeviceAdapter.setConnectedDeviceAddress(null)
                        }

                        /* if (connectedDevice?.name != null) {
                             Toast.makeText(context, "disconnected to Device" + connectedDevice?.name, Toast.LENGTH_SHORT).show()
                         } else {
                             Toast.makeText(context, "disconnected to Device" + connectedDevice?.address, Toast.LENGTH_SHORT).show()
                         }*/
                    }

                } else {
                    if (action.equals(ACTION_GATT_CONNECTED)) {
                        Toast.makeText(context, "Connected to Device", Toast.LENGTH_SHORT).show()
                    } else if (action.equals(ACTION_GATT_DISCONNECTED)) {
                        Toast.makeText(context, "Connected to Device", Toast.LENGTH_SHORT).show()
                    }
                }

                updateUI()
            }
            )
        }
    }


    fun updateUI() {
        dissmissDialog()
        if (bleDeviceAdapter.getConnectedDeviceAddress() != null) {
            connectBt.setText("Next")
            connectBt.visibility = View.VISIBLE;
        } else {
            connectBt.setText("Skip")
//            connectBt.visibility = View.GONE;
        }
    }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            mBluetoothLeService = (service as BluetoothService.LocalBinder).getService()
            if (!mBluetoothLeService?.initialize()!!) {
                Log.e(TAG, "Unable to initialize Bluetooth")
                finish()
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            mBluetoothLeService = null
        }
    }

    private lateinit var context: BleDeviceConnectionActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ble_connection)

        blueDeviceRv = findViewById(R.id.bleconnect_rv)
        connectBt = findViewById(R.id.bleconnect_bt_connect)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        context = this

        bleDeviceAdapter = BleDeviceAdapter(this, object : ItemClickListener {
            override fun onItemClick(positio: Int) {
//                connectDevice(bleDeviceAdapter.getBluetoothDeviceForPostion(positio)?.address!!)
                connectBt.performClick()
            }
        })
        var linearLayoutManager = LinearLayoutManager(this)
        blueDeviceRv.layoutManager = linearLayoutManager
        blueDeviceRv.adapter = bleDeviceAdapter

        val bluetoothManager =
                getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.getAdapter()

        checkForPermission()
        startBluetoothService()
        registerReceiver(mGattReceiver, makeGattUpdateIntentFilter())

        connectBt.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                if (sharedPreferences.getString("name", "").isEmpty()) {
                    val intent = Intent(context, SignInActivity::class.java)
                    startActivity(intent)
                } else {
                    val intent = Intent(context, HomeActivity::class.java)
                    startActivity(intent)
                }
            }
        })
    }

    private fun checkForPermission() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION)
        } else {
            checkbluetoothPermission()
        }
    }

    private fun checkbluetoothPermission() {
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        } else {
          checkForLocation()
        }
    }

    protected fun checkForLocation(){
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER)|| lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            scanForBleDevice(true)
        }
        else{
            turnOnDeviceLocationShowDialog()

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_LOCATION) {
            checkForPermission()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            checkForPermission()
        }
        else if(requestCode==REQUEST_LOCATION_SETTING){
            checkForPermission()
        }
    }

    private fun scanForBleDevice(enable: Boolean) {
        if (enable) {
            handler.postDelayed(object : Runnable {
                override fun run() {
                    mScanning = false
                    bluetoothAdapter.stopLeScan(mLeScanCallback)
                }
            }, SCAN_PERIOD)
            bluetoothAdapter.startLeScan(mLeScanCallback)
        } else {
            mScanning = false
            bluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private val mLeScanCallback = BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        runOnUiThread {
            if (device != null) {
                bleDeviceAdapter.addDevice(device)
                bleDeviceAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun connectDevice(deviceAddress: String) {
        // Automatically connects to the device upon successful start-up initialization.
        showProgressDialog("Connecting to device....")
        mBluetoothLeService?.connect(deviceAddress)
    }

    private fun startBluetoothService() {
        val gattServiceIntent = Intent(this, BluetoothService::class.java)
        bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun makeGattUpdateIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_GATT_CONNECTED)
        intentFilter.addAction(ACTION_GATT_DISCONNECTED)
        intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED)
        intentFilter.addAction(ACTION_DATA_AVAILABLE)
        return intentFilter
    }

    protected fun turnOnDeviceLocationShowDialog() {
        var alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("Resolve Disc wants to turn on Location")
        alertDialogBuilder.setPositiveButton("Setting", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                val viewIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(viewIntent,REQUEST_LOCATION_SETTING)
            }
        })

        alertDialogBuilder.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, p1: Int) {
                dialog?.dismiss()
              checkForPermission()
            }

        })
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.create().show()
    }
}