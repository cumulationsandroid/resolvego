package com.resolvegital.disc.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.resolvegital.disc.R

class ColorChooseActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.color_layout)

        findViewById<TextView>(R.id.red_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.blue_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.green_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.cyan_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.orange_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.yellow_tv).setOnClickListener(this)
        findViewById<TextView>(R.id.pink_tv).setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        var text: String? = null

        val intent = Intent()
        intent.putExtra("color", (v as TextView).text)
        intent.putExtra("textColor",(v as TextView).currentTextColor)
        setResult(Activity.RESULT_OK,intent)
        finish()
    }
}