package com.resolvegital.disc.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.resolvegital.disc.R
import com.resolvegital.disc.activity.ColorChooseActivity
import java.util.*


lateinit var colorTv: TextView
lateinit var usernameTv: TextView
lateinit var sharedPreferences: SharedPreferences

class SettingFragment : Fragment(), View.OnClickListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = LayoutInflater.from(activity).inflate(R.layout.setting_layout, container, false);
        ButterKnife.bind(this, view)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)

        usernameTv = view.findViewById(R.id.setting_tv_name)
        colorTv = view.findViewById(R.id.setting_color_tv)
        usernameTv.text = sharedPreferences.getString("name", "")

        view.findViewById<TextView>(R.id.setting_color_tv).setOnClickListener(this)
        view.findViewById<ImageView>(R.id.setting_iv_edit).setOnClickListener(this)

        return view;
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.setting_color_tv -> {
                launchColorEditorActivity()
            }
            R.id.setting_iv_edit -> {
                showUsernameEditDialog()
            }
        }
    }

    fun launchColorEditorActivity() {
        var intent = Intent(activity, ColorChooseActivity::class.java)
        startActivityForResult(intent, 111)
    }

    fun showUsernameEditDialog() {
        var alertDialogBuilder = AlertDialog.Builder(activity)
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setTitle("Edit Username")
        var usernameEt = EditText(activity)
        var lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        usernameEt.setTextColor(Color.BLACK)
        usernameEt.setTextSize(20f)
        usernameEt.layoutParams = lp;

        alertDialogBuilder.setView(usernameEt)

        alertDialogBuilder.setPositiveButton(R.string.ok, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (!usernameEt.text.toString().toString().isEmpty()) {
                    sharedPreferences.edit().putString("name", usernameEt.text.toString()).apply()
                    usernameTv.setText(sharedPreferences.getString("name", ""))
                    dialog?.dismiss()
                }
            }
        })

        alertDialogBuilder.setNegativeButton(R.string.cancel, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })

        var alertdialog: AlertDialog
        alertdialog = alertDialogBuilder.create()
        alertdialog.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                var colorValue = data.getIntExtra("textColor", Color.BLACK)
                var colorName=data.getStringExtra("color")
                colorTv.setText(colorName)
                colorTv.setTextColor(colorValue)
            }
        }
    }
}