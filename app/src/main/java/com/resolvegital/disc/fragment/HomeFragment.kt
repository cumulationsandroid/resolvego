package com.resolvegital.disc.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.resolvegital.disc.R

class HomeFragment :Fragment(),View.OnClickListener
{
    lateinit var nameTv:TextView
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view=LayoutInflater.from(activity).inflate(R.layout.fragment_home,container,false)
        sharedPreferences=PreferenceManager.getDefaultSharedPreferences(activity)
        nameTv=view.findViewById(R.id.home_name_tv)

        nameTv.text = "Hey "+sharedPreferences.getString("name","")+" !!"
        return view
    }

    override fun onClick(v: View?) {

    }
}