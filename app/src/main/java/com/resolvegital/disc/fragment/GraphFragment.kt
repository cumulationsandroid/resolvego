package com.resolvegital.disc.fragment

import android.graphics.*
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.github.mikephil.charting.charts.BarChart
import com.resolvegital.disc.R
import java.util.*
import kotlin.collections.ArrayList
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ViewPortHandler
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider
import com.github.mikephil.charting.renderer.AxisRenderer
import com.github.mikephil.charting.renderer.BarChartRenderer
import com.github.mikephil.charting.renderer.XAxisRendererHorizontalBarChart
import com.github.mikephil.charting.utils.Utils
import com.resolvegital.disc.util.Month

class GraphFragment : Fragment(), View.OnClickListener {
    lateinit var monthDayTv: TextView
    lateinit var yearMonthTv: TextView
    lateinit var mChart: BarChart
    lateinit var mChart2: BarChart

    lateinit var monthYearTv: TextView

    private var year: String = getCurrentYEAR()
    private var month: String = getCurrentMonth()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = LayoutInflater.from(activity).inflate(R.layout.graph_layout, container, false)

        monthDayTv = view.findViewById<TextView>(R.id.graph_tv_month)
        yearMonthTv = view.findViewById<TextView>(R.id.graph_tv_year)
        mChart = view.findViewById(R.id.graph_bar_chart)
        mChart2 = view.findViewById(R.id.graph_bar_chart2)
        monthYearTv = view.findViewById(R.id.graph_tv_monthyear)

        monthDayTv.setOnClickListener(this)
        yearMonthTv.setOnClickListener(this)

        view.findViewById<ImageView>(R.id.previous_iv).setOnClickListener(this)
        view.findViewById<ImageView>(R.id.next_iv).setOnClickListener(this)

        monthDayTv.performClick()
        return view;
    }

    override fun onResume() {
        super.onResume()
        udateUI()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.graph_tv_month -> {
                yearMonthTv.isSelected = false
                monthDayTv.isSelected = true

            }
            R.id.graph_tv_year -> {
                monthDayTv.isSelected = false
                yearMonthTv.isSelected = true
            }
            R.id.previous_iv -> {
                if (yearMonthTv.isSelected) {
                    year = (monthYearTv.text.toString().toInt() - 1).toString()
                } else {
                    if (monthYearTv.text.toString().equals(Month.JANUARY.monthValue, true)) {
                        return
                    }
                    month = Month.getPreviousMonth(monthYearTv.text.toString())
                }
            }

            R.id.next_iv -> {
                if (yearMonthTv.isSelected) {
                    if (!year.equals(getCurrentYEAR())) {
                        year = (monthYearTv.text.toString().toInt() + 1).toString()
                    } else {
                        return
                    }
                } else {
                    if (month.equals(getCurrentMonth(), true) && year.equals(getCurrentYEAR())) {
                        return
                    } else {
                        month = Month.getNextMonth(monthYearTv.text.toString())
                    }
                }
            }
        }
        updateUi()
    }

    private fun updateUi() {
        if (monthDayTv.isSelected) {
            monthYearTv.setText(month)
            udateUI()
            mChart.visibility = View.VISIBLE
            mChart2.visibility = View.GONE
        } else {
            monthYearTv.setText(year)
            udateUI2()
            mChart.visibility = View.GONE
            mChart2.visibility = View.VISIBLE
        }
    }

    fun numberOfDaysInMonth(month: Int, year: Int): Int {
        val monthStart = GregorianCalendar(year, month, 1)
        return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH)
    }

    fun getCurrentMonth(): String {
        val calendar = Calendar.getInstance()

        return (Month.values()[(calendar.time.month)]).monthValue
    }

    fun getCurrentYEAR(): String {
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.YEAR).toString()
    }

    private fun getGraphDataForMonthDay(): ArrayList<BarEntry> {
        var min = 1
        var max = 30

        if (monthDayTv.isSelected) {
            if (monthYearTv.text.toString().equals(getCurrentMonth(), true)) {
                max = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            } else {
                max = numberOfDaysInMonth(Month.getInDex(month), year.toInt())
            }
        }

        val entries = ArrayList<BarEntry>()

        for (i in 1..max) {
            val random = Random().nextInt(max - min + 1) + min
            var entry = BarEntry(i.toFloat(), random.toFloat())
            entries.add(entry)
        }

        return entries
    }

    private fun getGraphDataForYearMOnth(): ArrayList<BarEntry> {
        val min = 1
        var max = 12

        if (yearMonthTv.isSelected && year.equals(getCurrentYEAR())) {
            max = Calendar.getInstance().get(Calendar.MONTH) + 1
        }

        val entries = ArrayList<BarEntry>()

        for (i in 1..max) {
            val random = Random().nextInt(max - min + 1) + min
            var entry = BarEntry(i.toFloat(), random.toFloat())
            entries.add(entry)
        }

        return entries
    }

    fun udateUI2() {
        mChart2.clear()
        mChart2.invalidate()
        mChart2.fitScreen()

        mChart2.post(object : Runnable {
            override fun run() {

                if (yearMonthTv.isSelected) {
                    val datasetList = getGraphDataForYearMOnth()
                    var bardataset = BarDataSet(datasetList, "Cells")

                    bardataset.setDrawValues(false)
                    var data = BarData(bardataset)

                    mChart2.setData(data)

                    mChart2.xAxis.valueFormatter = object : IAxisValueFormatter {
                        override fun getFormattedValue(value: Float, axis: AxisBase?): String {

                            val month = (Month.values()[(value - 1).toInt()]).monthValue
                            if (month.length > 4) {
                                return month.substring(0, 3)
                            }
                            return month
                        }
                    }
                }

                mChart2.xAxis.setCenterAxisLabels(false)
                mChart2.xAxis.setLabelCount(12)
                val xAxis = mChart2.getXAxis()
                xAxis.setGranularity(1f)
                xAxis.setGranularityEnabled(true)

//        val drawable = ContextCompat.getDrawable(this, R.drawable.grapgh_gradient)
//                mChart.setRenderer(CustomRender(mChart, mChart.getAnimator(), mChart.getViewPortHandler()))

                val mPaint = mChart2.getRenderer().getPaintRender()
                mPaint.shader = LinearGradient(0f, 0f, 0f, (mChart.bottom).toFloat(),
                        Color.parseColor("#806A82FB"), Color.parseColor("#80FC5C7D"), Shader.TileMode.MIRROR)

                mChart2.setDrawGridBackground(false)
                mChart2.setPinchZoom(false)
                mChart2.isDoubleTapToZoomEnabled = false
                var yAxis = mChart2.axisLeft
                yAxis.setDrawGridLines(false)
                yAxis.axisMinimum = 0f

                yAxis = mChart2.axisRight
                yAxis.setDrawGridLines(false)

                mChart2.axisLeft.isEnabled = false
                mChart2.axisRight.isEnabled = false
                mChart2.xAxis.setDrawGridLines(false)
                mChart2.xAxis.position = XAxis.XAxisPosition.BOTTOM
                mChart2.description.isEnabled = false
                val leg = mChart2.legend
                leg.isEnabled = false

                mChart2.getAxisLeft().setDrawLabels(false);
                mChart2.getAxisRight().setDrawLabels(false);

                mChart2.xAxis.textColor = Color.parseColor("#3C0B60")
                mChart2.xAxis.axisLineColor = Color.parseColor("#3C0B60")
                mChart2.isScaleXEnabled = true
                mChart2.isScaleYEnabled = false
                mChart2.invalidate()
            }
        })
    }


    fun udateUI() {
        mChart.clear()
        mChart.fitScreen()
        mChart.invalidate()

        mChart.post(object : Runnable {
            override fun run() {
                mChart.clear()
                mChart.invalidate()

                if (monthDayTv.isSelected) {
                    val datasetList = getGraphDataForMonthDay()
                    var bardataset = BarDataSet(getGraphDataForMonthDay(), "Cells")

                    bardataset.setDrawValues(false)

                    var data = BarData(bardataset)

                    mChart.setData(data)

                    mChart.xAxis.valueFormatter = object : IAxisValueFormatter {
                        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
                            return value.toInt().toString()
                        }
                    }

                }

                mChart.getXAxis().setCenterAxisLabels(false)
                mChart.setVisibleXRangeMaximum(7f)
                val xAxis = mChart.getXAxis()
                xAxis.setGranularity(1f)
                xAxis.setGranularityEnabled(true)

//        val drawable = ContextCompat.getDrawable(this, R.drawable.grapgh_gradient)
//                mChart.setRenderer(CustomRender(mChart, mChart.getAnimator(), mChart.getViewPortHandler()))

                val mPaint = mChart.getRenderer().getPaintRender()
                mPaint.shader = LinearGradient(0f, 0f, 0f, (mChart.bottom).toFloat(),
                        Color.parseColor("#806A82FB"), Color.parseColor("#80FC5C7D"), Shader.TileMode.MIRROR)

                mChart.setDrawGridBackground(false)
                mChart.setPinchZoom(false)
                mChart.isDoubleTapToZoomEnabled = false
                var yAxis = mChart.axisLeft
                yAxis.setDrawGridLines(false)
                yAxis.axisMinimum = 0f

                yAxis = mChart.axisRight
                yAxis.setDrawGridLines(false)

                mChart.axisLeft.isEnabled = false
                mChart.axisRight.isEnabled = false
                mChart.xAxis.setDrawGridLines(false)
                mChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
                mChart.description.isEnabled = false
                val leg = mChart.legend
                leg.isEnabled = false

                mChart.getAxisLeft().setDrawLabels(false)
                mChart.getAxisRight().setDrawLabels(false)

                mChart.xAxis.textColor = Color.parseColor("#3C0B60")
                mChart.xAxis.axisLineColor = Color.parseColor("#3C0B60")
                mChart.isScaleXEnabled = true
                mChart.isScaleYEnabled = false
                mChart.invalidate()

                mChart.postDelayed(Runnable {
                    try {
                        mChart.moveViewToX(mChart.barData.entryCount - 6.5f);
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, 10)
            }
        })

        // mChart.setDrawYLabels(false);

    }

    private inner class CustomRender(chart: BarDataProvider, animator: ChartAnimator, viewPortHandler: ViewPortHandler) : BarChartRenderer(chart, animator, viewPortHandler) {

        private val mBarShadowRectBuffer = RectF()

        protected override fun drawDataSet(c: Canvas, dataSet: IBarDataSet, index: Int) {

            val trans = mChart.getTransformer(dataSet.axisDependency)

            mBarBorderPaint.color = dataSet.barBorderColor
            mBarBorderPaint.strokeWidth = Utils.convertDpToPixel(dataSet.barBorderWidth)

            val drawBorder = dataSet.barBorderWidth > 0f

            val phaseX = mAnimator.phaseX
            val phaseY = mAnimator.phaseY

            // draw the bar shadow before the values
            if (mChart.isDrawBarShadowEnabled) {
                mShadowPaint.color = dataSet.barShadowColor

                val barData = mChart.barData

                val barWidth = barData.barWidth
                val barWidthHalf = barWidth / 2.0f
                var x: Float

                var i = 0
                val count = Math.min(Math.ceil((dataSet.entryCount.toFloat() * phaseX).toDouble()).toInt(), dataSet.entryCount)
                while (i < count) {

                    val e = dataSet.getEntryForIndex(i)

                    x = e.x

                    mBarShadowRectBuffer.left = x - barWidthHalf
                    mBarShadowRectBuffer.right = x + barWidthHalf

                    trans.rectValueToPixel(mBarShadowRectBuffer)

                    if (!mViewPortHandler.isInBoundsLeft(mBarShadowRectBuffer.right)) {
                        i++
                        continue
                    }

                    if (!mViewPortHandler.isInBoundsRight(mBarShadowRectBuffer.left))
                        break

                    mBarShadowRectBuffer.top = mViewPortHandler.contentTop()
                    mBarShadowRectBuffer.bottom = mViewPortHandler.contentBottom()

                    c.drawRoundRect(mBarShadowRectBuffer, 100f, 200f, mShadowPaint)
                    i++
                }
            }

            // initialize the buffer
            initBuffers()
            val buffer = mBarBuffers[index]
            buffer.setPhases(phaseX, phaseY)
            buffer.setDataSet(index)
            buffer.setInverted(mChart.isInverted(dataSet.axisDependency))
            buffer.setBarWidth(mChart.barData.barWidth)

            buffer.feed(dataSet)

            trans.pointValuesToPixel(buffer.buffer)

            val isSingleColor = dataSet.colors.size == 1

            if (isSingleColor) {
                mRenderPaint.color = dataSet.color
            }

            var j = 0
            while (j < buffer.size()) {

                if (!mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2])) {
                    j += 4
                    continue
                }

                if (!mViewPortHandler.isInBoundsRight(buffer.buffer[j]))
                    break

                if (!isSingleColor) {
                    // Set the color for the currently drawn value. If the index
                    // is out of bounds, reuse colors.
                    mRenderPaint.color = dataSet.getColor(j / 4)
                }

                c.drawRoundRect(RectF(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                        buffer.buffer[j + 3]), 30f, 30f, mRenderPaint)

                if (drawBorder) {
                    c.drawRect(buffer.buffer[j], buffer.buffer[j + 1], buffer.buffer[j + 2],
                            buffer.buffer[j + 3], mBarBorderPaint)
                }
                j += 4
            }
        }
    }
}