package com.resolvegital.disc.util

enum class Month(month: String) {

    JANUARY("January"),
    FEBRUARY("February"),
    MARCH("March"),
    APIRL("Apirl"),
    MAY("May"),
    JUNE("June"),
    JULY("July"),
    AUGUST("August"),
    SEPTEMBER("September"),
    OCTOBER("October"),
    NOVEMBER("November"),
    DECEMBER("December");

    val monthValue: String = month

    companion object {
        public fun getNextMonth(value: String): String {
            Month.values().forEachIndexed { index, month ->
                if (month.monthValue.equals(value, true)) {
                    if (index == Month.values().size - 1) {
//                      return Month.JANUARY.monthValue
                    } else {
                        return (Month.values()[(index + 1)]).monthValue
                    }
                }
            }
            return value
        }

        public fun getPreviousMonth(value: String): String {
            Month.values().forEachIndexed { index, month ->
                if (month.monthValue.equals(value, true)) {
                    if (index == 0) {
//                    return  Month.DECEMBER.monthValue
                    } else {
                        return (Month.values()[(index - 1)]).monthValue
                    }
                }
            }
            return value
        }

        public fun getInDex(value: String): Int {

            Month.values().forEachIndexed { index, month ->
                if (month.monthValue.equals(value, true)) {
                    return index
                }
            }

            return 30
        }
    }
    }